/*
 * @file    : myStack.c
 * @author  : Emiliano Jofre
 * @date    : 23/06/2021
 * @brief   : Tarea1 - EDA
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "myStack.h"

//IMPLEMENTACION FUNCIONES
int countNodes(struct node *head){ //cuenta los nodos de una lista enlazada
    int count=0;
    if(head==NULL){
        return count;
    }
    Student *ptr = head;
    while(ptr != NULL){
        count++;
        ptr=ptr->next;
    }
    return count;
}
int isFull (Stack* stack){ //retorna 0 para false y 1 para true //indica si el stack st está lleno
    if(stack==NULL){
        fprintf (stderr, "NULL pointer passed to isFull().\n");
        return 0;
    }
    else{
        if(countNodes(stack->top)==(stack->capacity)){
            return 1;
        }
        return 0;
    }
}
int isEmpty(Stack* stack){ //retorna 0 para false y 1 para true //indica si el stack st esta vacio
    if(stack->top==NULL || countNodes(stack->top)==0){
        return 1;
    }else{
        return 0;
    }
}
Stack* getNewStack (unsigned int size){
    Stack *newStack = NULL;
    if(size>0){
        newStack = (Stack*)malloc(sizeof(Stack));
        //Control si malloc no funciona
        if(newStack==NULL){
            fprintf (stderr, "Error creating a new Stack with malloc.\n");
        }else{
            newStack->top=NULL;
            newStack->capacity=size;
        }
    }
    return newStack;
}
void addFirst(Student **head, Student *student){
    //printf("%s,%s,%i\n",nombre,apellido,nota); //Para comprobar que se esta recibiendo bien los datos
    Student *newnode = (Student*)malloc(sizeof(Student));
    newnode->nombre = student->nombre;
    newnode->apellido = student->apellido;
    newnode->nota=student->nota;
    newnode->previous=NULL;
    if(*head==NULL){ //Si esta vacia
        //printf("%s,%s,%i\n",newnode->nombre,newnode->apellido,newnode->nota);
        newnode->next = NULL;
        *head=newnode;
    }else{ //Si no esta vacia
        //printf("%s,%s,%i\n",newnode->nombre,newnode->apellido,newnode->nota);
        (*head)->previous=newnode;
        newnode->next=*head;
        *head=newnode;
    }
    //printf("%s,%s,%i\n", newnode->nombre,newnode->apellido,newnode->nota); //Para comprobar que se esta recibiendo bien los datos
}
void push(Student **head, Student *student){
    //printf("%s,%s,%i\n",student->nombre,student->apellido,student->nota);
    addFirst(head,student);
}