/*
 * @file    : myStack.h
 * @author  : Emiliano Jofre
 * @date    : 23/06/2021
 * @brief   : Tarea1 - EDA
 */
#ifndef MYSTACK_H
#define MYSTACK_H

//LISTA DOBLEMENTE ENLAZADA
typedef struct node{
    char* nombre;
    char* apellido;
    int nota;
    struct node* previous;
    struct node* next;
}Student;

//STACK
typedef struct stack {
    Student* top; //puntero al primer elemento de la lista enlazada
    unsigned int capacity;
}Stack;

//FUNCIONES
int countNodes(struct node *head);
int isFull (Stack* stack); // indica si el stack esta lleno
int isEmpty(Stack* stack); // indica si el stack esta vacio
Stack* getNewStack (unsigned int size); // genera un nuevo stack de capacidad size
int stackDelete(); //elimina el stack
void addFirst(Student **head, Student *student);
void push(Student **head, Student *student); //agrega el elemento al stack st
Student* pop(Stack* stack); //quita el primero elemento del stack

#endif	// MYSTACK_H