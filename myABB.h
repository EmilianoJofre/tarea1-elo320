/*
 * @file    : myABB.h
 * @author  : Emiliano Jofre
 * @date    : 23/06/2021
 * @brief   : Tarea1 - EDA
 */
#ifndef MYABB_H
#define MYABB_H

//ARBOL BINARIO DE BUSQUEDA
typedef struct moldenodo{
    char* nombre;
    char* apellido;
    int nota;
    struct moldenodo* left;
    struct moldenodo* right;
}StudentABB;

//FUNCIONES ABB
StudentABB *getNewNode(char *nombre, char *apellido, int nota);
void insertStudent(StudentABB **root,char *nombre, char *apellido, int nota);
void printRecursive(StudentABB *root);


#endif	// MYABB_H