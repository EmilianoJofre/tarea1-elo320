/*
 * @file    : myABB.c
 * @author  : Emiliano Jofre
 * @date    : 23/06/2021
 * @brief   : Tarea1 - EDA
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "myABB.h"

//IMPLEMENTACION FUNCIONES
StudentABB *getNewNode(char *nombre, char *apellido, int nota){
    StudentABB *newnode = (StudentABB*)malloc(sizeof(StudentABB));
    newnode->nombre=nombre;
    newnode->apellido=apellido;
    newnode->nota=nota;
    newnode->left=NULL;
    newnode->right=NULL;
    return newnode;
}
void insertStudent(StudentABB **root,char *nombre, char *apellido, int nota){
    StudentABB *newnode = getNewNode(nombre,apellido,nota);
    if(*root==NULL){ //Si el arbol esta vacio
        (*root)=newnode;
    }else{ //Si no esta vacio
        StudentABB *prev,*temp;
        prev=NULL;
        temp=*root;
        while(temp!=NULL){
            prev = temp;
            if(strcmp(apellido,temp->apellido)>0){
                temp = temp->right;
            }else{
                temp = temp->left;
            }
        }
        if(strcmp(apellido,prev->apellido)>0){
            prev->right=newnode;
        }else{
            prev->left=newnode;
        }
    }
}
void printRecursive(StudentABB *root){
    if(root!=NULL){
        printRecursive(root->left);
        printf("%s,%s,%i\n",root->nombre,root->apellido,root->nota);
        printRecursive(root->right);
    }
}

