/*
 * @file    : main.c
 * @author  : Emiliano Jofre
 * @date    : 23/06/2021
 * @brief   : Tarea1 - EDA
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "myStack.h"
#include "myABB.h"

#define ROWS 2
#define COLS 26
#define stackSIZE 26

//DECLARACION DE FUNCIONES
void initEncryptionTable(char table[ROWS][COLS]);
void encrypt(char* str, char table[ROWS][COLS]);
char toMayus(char c);
char toMinus(char c);
void decrypt(char* str, char table[ROWS][COLS]);
void readFile(char *path, char *mode, char table[ROWS][COLS]);
void dirtyEncryptedWork(Stack *stack);
void dirtyWork(Stack *stack);
void decryptNodes(Student *head,char table[ROWS][COLS]);
void printStudent(Student* student);
void printApproved(Stack* stack);
void printBestNota(Stack* stack);

//DECLARACION DE FUNCIONES LISTA ENLAZADA
Student* getNewStudent(); //crea un nuevo Student
void printNodes(Student *head);

int main(int argc, char* argv[]){
    //Argument control logic
    if(argc == 2){
        printf("The argument supplied is %s\n", argv[1]); //One argument
    }else if(argc>2){
        printf("Too many argument expected. \n"); //Too many argument's
    }else{
        printf("One argument expected. \n"); //No argument's
    }
    //Se inicializa la tabla de encriptacion
    char encryptionTable[ROWS][COLS];
    initEncryptionTable(encryptionTable);

    //Se crea un nuevo stack
    Stack *stackEncrypted = getNewStack(stackSIZE);
    Stack *stack = getNewStack(stackSIZE);

    //Se lee el archivo
    readFile(argv[1],"r",encryptionTable);

    //Se agregan los estudiantes al stack
    dirtyWork(stack);
    dirtyEncryptedWork(stackEncrypted);

    //Se imprimen ls resultados
    printNodes(stackEncrypted->top);
    //decryptNodes(stack->top,encryptionTable);
    printApproved(stack);
    printBestNota(stack);

    return 0;
}

//IMPLEMENTACION DE FUNCIONES
void initEncryptionTable(char table[ROWS][COLS]){
    table[0][0] = 'A'; //comenzamos llenando la primera fila con las letras del abecedario en orden normal
    int i;
    for(i=1;i<COLS;i++){ //utilizando un bucle for
        table[0][i] = table[0][i-1]+1; //llenamos toda la primera fila con las letras correspondientes
    }
    for(i=0;i<COLS;i++){ //copiamos en la segunda fila el contenido de la primera fila
        table[1][i] = table[0][i];
    }
    int random;
    char buffer; //variable utilizada para intercambiar valores
    for(i=0;i<COLS;i++){
        do{
            random = rand() % (COLS); //numero aleatorio entre 0 y 25
            buffer = table[1][i];
            table[1][i] = table[1][random];
            table[1][random] = buffer;
        } while (table[1][i]==table[0][i] || table[1][random]==table[0][random]); //para asegurarse de cumplir el requisito de encriptacion
    }
    //PARA COMPROBAR QUE LA TABLA SE INICIALIZA BIEN
    /*for(i=0;i<COLS;i++){
        printf("%c",table[0][i]);
    }
    printf("\n");
    for(i=0;i<COLS;i++){
        printf("%c",table[1][i]);
    }*/

}
char toMayus(char c){ //Funcion que permite convertir c en mayuscula
    return c - 'a' + 'A';
}
char toMinus(char c){ //Funcion que permite convertir c en minuscula
    return c - 'A' + 'a';
}
void encrypt(char *str, char table[ROWS][COLS]){
    int i;
    for(i=0;i<strlen(str);i++){
        if(str[i] >= 'a' && str[i] <= 'z'){ //Si es letra minuscula
            str[i] = toMayus(str[i]); //La transformamos en mayuscula
            int j;
            for(j=0;j<COLS;j++){ //La intercambiamos por su equivalente segun la tabla
                if(str[i]==table[0][j]){ //Si encontramos la letra actual en el abecedario
                    str[i]=table[1][j]; //La intercambiamos por la letra equivalente segun la tabla de encriptacion
                    break; //Nos salimos del bucle
                }
            }
            str[i]= toMinus(str[i]); //Y la volvemos a transformar en minuscula
        }else if(str[i] >= 'A' && str[i] <= 'Z'){ //Si es letra mayuscula
            int j;
            for(j=0;j<COLS;j++){ //La intercambiamos por su equivalente segun la tabla
                if(str[i]==table[0][j]){ //Si encontramos la letra actual en el abecedario
                    str[i]=table[1][j]; //La intercambiamos por la letra equivalente segun la tabla de encriptacion
                    break; //Nos salimos del bucle
                }
            }
        }
    }
}

void decrypt(char* str, char table[ROWS][COLS]){
    int i;
    for(i=0;i<strlen(str);i++){
        if(str[i] >= 'a' && str[i] <= 'z'){ //Si es letra minuscula
            str[i] = toMayus(str[i]); //La transformamos en mayuscula
            int j;
            for(j=0;j<COLS;j++){ //La intercambiamos por su equivalente segun la tabla
                if(str[i]==table[1][j]){ //Si encontramos la letra actual en el abecedario
                    str[i]=table[0][j]; //La intercambiamos por la letra equivalente segun la tabla de encriptacion
                    break; //Nos salimos del bucle
                }
            }
            str[i]= toMinus(str[i]); //Y la volvemos a transformar en minuscula
        }else if(str[i] >= 'A' && str[i] <= 'Z'){ //Si es letra mayuscula
            int j;
            for(j=0;j<COLS;j++){ //La intercambiamos por su equivalente segun la tabla
                if(str[i]==table[1][j]){ //Si encontramos la letra actual en el abecedario
                    str[i]=table[0][j]; //La intercambiamos por la letra equivalente segun la tabla de encriptacion
                    break; //Nos salimos del bucle
                }
            }
        }
    }
}
void readFile(char *path, char *mode,char table[ROWS][COLS]){
    FILE *fp = fopen(path,mode); //Se abre el archivo para leer su contenido
    char buffer[30];
    int n=1;
    while(!feof(fp)){ //mientras no llegemos al final del archivo
        fgets(buffer,30,fp); //Obtenemos el texto del archivo y lo guardamos momentaneamente en el buffer
        //encrypt(buffer,table); //Se encripta el nombre y apellido
        //printf("%s",buffer);
        //printf("Student *student%i = (Student*)malloc(sizeof(Student));\n",n);
        int i;
        char delimiter[]=", \n\0";
        char *token = strtok(buffer,delimiter);
        Student *newstudent = (Student*)malloc(sizeof(Student));
        if(token!=NULL){
            while(token!=NULL){
                for(i=0;i<3;i++){
                    //printf("%s\n",token);
                    if(i==0){
                        newstudent->nombre=token;
                        //printf("student%i->nombre='%s';\n",n,newstudent->nombre);
                    }else if(i==1){
                        newstudent->apellido=token;
                        //printf("student%i->apellido='%s'';\n",n,newstudent->apellido);
                    }else{
                        newstudent->nota=(int)strtol(token,NULL,10);
                        //printf("student%i->nota=%i;\n",n,newstudent->nota);
                    }
                    token = strtok(NULL,delimiter);
                }
                //printStudent(newstudent);
                //push(&(stack->top),newstudent);
            }
        }
        n++;
    }
    int i;
    for(i=1;i<=25;i++){
        printf("push(&(stack2->top),student%i);\n",i);
    }
}
void decryptNodes(Student *head,char table[ROWS][COLS]){
    Student *temp = head;
    while(temp!=NULL){
        decrypt(temp->nombre,table);
        decrypt(temp->apellido,table);
        temp=temp->next;
    }
}
void printStudent(Student* student){
    printf("%s,%s,%i\n",student->nombre,student->apellido,student->nota);
}
void printApproved(Stack* stack){
    printf("\n\n**Se imprimen los alumnos aprobados**\n\n");
    Student *temp = stack->top;
    int i;
    for(i=0;i<countNodes(stack->top);i++){
        if(temp->nota>55){
            printStudent(temp);
        }
        temp=temp->next;
    }
}
void printBestNota(Stack* stack){
    printf("\n\n**Se imprime el alumno con la mejor nota**\n\n");
    Student *temp = stack->top;
    int i,best=0;
    for(i=0;i<countNodes(stack->top);i++){
        if(temp->nota>best){
            best=temp->nota;
        }
        temp=temp->next;
    }
    Student *temp2 = stack->top;
    for(i=0;i<countNodes(stack->top);i++){
        if(temp2->nota==best){
            printStudent(temp2);
        }
        temp2=temp2->next;
    }

}
//IMPLEMENTACION DE FUNCIONES LISTA ENLAZADA
Student* getNewStudent(){
    Student* newnode = (Student*)malloc(sizeof(Student));
    newnode->next=NULL;
    newnode->previous=NULL;
    return newnode;
}
void printNodes(Student *head){
    printf("\n\n**Se imprimen todos los nodos**\n\n");
    Student *currPtr = head;
    while(currPtr!=NULL){
        printf("%s,%s,%i\n",currPtr->nombre,currPtr->apellido,currPtr->nota);
        currPtr=currPtr->next;
    }
}
//Trabajo sucio al no haber podido solucionar el error

void dirtyEncryptedWork(Stack *stack){
    Student *student1 = (Student*)malloc(sizeof(Student));
    student1->nombre="Hjkdjg";
    student1->apellido="Gaiakcg";
    student1->nota=54;
    Student *student2 = (Student*)malloc(sizeof(Student));
    student2->nombre="Tjfbxt";
    student2->apellido="Ekynm";
    student2->nota=0;
    Student *student3 = (Student*)malloc(sizeof(Student));
    student3->nombre="Sjs";
    student3->apellido="Thxbf";
    student3->nota=65;
    Student *student4 = (Student*)malloc(sizeof(Student));
    student4->nombre="Mbekcqcj";
    student4->apellido="Xyvbg";
    student4->nota=80;
    Student *student5 = (Student*)malloc(sizeof(Student));
    student5->nombre="Tcncekc";
    student5->apellido="Fbfjmjyxjg";
    student5->nota=90;
    Student *student6 = (Student*)malloc(sizeof(Student));
    student6->nombre="Pygecf";
    student6->apellido="Ecnsakxbva";
    student6->nota=30;
    Student *student7 = (Student*)malloc(sizeof(Student));
    student7->nombre="Bferjfh";
    student7->apellido="Gebkv";
    student7->nota=100;
    Student *student8 = (Student*)malloc(sizeof(Student));
    student8->nombre="Bxsake";
    student8->apellido="Acfgeacf";
    student8->nota=50;
    Student *student9 = (Student*)malloc(sizeof(Student));
    student9->nombre="Paffciak";
    student9->apellido="Bfcgejf";
    student9->nota=60;
    Student *student10 = (Student*)malloc(sizeof(Student));
    student10->nombre="Nbwoaxx";
    student10->apellido="Gnbke";
    student10->nota=44;
    Student *student11 = (Student*)malloc(sizeof(Student));
    student11->nombre="Tobhfa";
    student11->apellido="Pjrfgjf";
    student11->nota=46;
    Student *student12 = (Student*)malloc(sizeof(Student));
    student12->nombre="Qjfgebfecfjg";
    student12->apellido="Tbgvbxbvcg";
    student12->nota=100;
    Student *student13 = (Student*)malloc(sizeof(Student));
    student13->nombre="Ubrb";
    student13->apellido="Rbtct";
    student13->nota=71;
    Student *student14 = (Student*)malloc(sizeof(Student));
    student14->nombre="Qxbyta";
    student14->apellido="Njfae";
    student14->nota=61;
    Student *student15 = (Student*)malloc(sizeof(Student));
    student15->nombre="Mcakka-Bydyge";
    student15->apellido="Kafjck";
    student15->nota=53;
    Student *student16 = (Student*)malloc(sizeof(Student));
    student16->nombre="Bydygea";
    student16->apellido="Kjtcf";
    student16->nota=77;
    Student *student17 = (Student*)malloc(sizeof(Student));
    student17->nombre="Fcqbfjk";
    student17->apellido="Mbkkb";
    student17->nota=88;
    Student *student18 = (Student*)malloc(sizeof(Student));
    student18->nombre="Qrbkxag";
    student18->apellido="Sbssbda";
    student18->nota=75;
    Student *student19 = (Student*)malloc(sizeof(Student));
    student19->nombre="Nbkcj";
    student19->apellido="Safataeec";
    student19->nota=68;
    Student *student20 = (Student*)malloc(sizeof(Student));
    student20->nombre="Lcqafea";
    student20->apellido="Ryctjskj";
    student20->nota=90;
    Student *student21 = (Student*)malloc(sizeof(Student));
    student21->nombre="Nbkqax";
    student21->apellido="Mkjyge";
    student21->nota=53;
    Student *student22 = (Student*)malloc(sizeof(Student));
    student22->nombre="Axjf";
    student22->apellido="Nygv";
    student22->nota=100;
    Student *student23 = (Student*)malloc(sizeof(Student));
    student23->nombre="Iajtjk";
    student23->apellido="Tjgejhalgvc";
    student23->nota=49;
    Student *student24 = (Student*)malloc(sizeof(Student));
    student24->nombre="Dajkdcjg";
    student24->apellido="Mbmbfcvjxbjy";
    student24->nota=89;
    Student *student25 = (Student*)malloc(sizeof(Student));
    student25->nombre="Iatakcqj";
    student25->apellido="GbfebNbkcb";
    student25->nota=55;
    push(&(stack->top),student1);
    push(&(stack->top),student2);
    push(&(stack->top),student3);
    push(&(stack->top),student4);
    push(&(stack->top),student5);
    push(&(stack->top),student6);
    push(&(stack->top),student7);
    push(&(stack->top),student8);
    push(&(stack->top),student9);
    push(&(stack->top),student10);
    push(&(stack->top),student11);
    push(&(stack->top),student12);
    push(&(stack->top),student13);
    push(&(stack->top),student14);
    push(&(stack->top),student15);
    push(&(stack->top),student16);
    push(&(stack->top),student17);
    push(&(stack->top),student18);
    push(&(stack->top),student19);
    push(&(stack->top),student20);
    push(&(stack->top),student21);
    push(&(stack->top),student22);
    push(&(stack->top),student23);
    push(&(stack->top),student24);
    push(&(stack->top),student25);
}
void dirtyWork(Stack *stack2){
    Student *student1 = (Student*)malloc(sizeof(Student));
    student1->nombre="Yorgos";
    student1->apellido="Seferis";
    student1->nota=54;
    Student *student2 = (Student*)malloc(sizeof(Student));
    student2->nombre="Donald";
    student2->apellido="Trump";
    student2->nota=0;
    Student *student3 = (Student*)malloc(sizeof(Student));
    student3->nombre="Bob";
    student3->apellido="Dylan";
    student3->nota=65;
    Student *student4 = (Student*)malloc(sizeof(Student));
    student4->nombre="Patricio";
    student4->apellido="Lukas";
    student4->nota=80;
    Student *student5 = (Student*)malloc(sizeof(Student));
    student5->nombre="Dimitri";
    student5->apellido="Nanopoulos";
    student5->nota=90;
    Student *student6 = (Student*)malloc(sizeof(Student));
    student6->nombre="Justin";
    student6->apellido="Timberlake";
    student6->nota=30;
    Student *student7 = (Student*)malloc(sizeof(Student));
    student7->nombre="Anthony";
    student7->apellido="Stark";
    student7->nota=100;
    Student *student8 = (Student*)malloc(sizeof(Student));
    student8->nombre="Albert";
    student8->apellido="Einstein";
    student8->nota=50;
    Student *student9 = (Student*)malloc(sizeof(Student));
    student9->nombre="Jennifer";
    student9->apellido="Aniston";
    student9->nota=60;
    Student *student10 = (Student*)malloc(sizeof(Student));
    student10->nombre="Maxwell";
    student10->apellido="Smart";
    student10->nota=44;
    Student *student11 = (Student*)malloc(sizeof(Student));
    student11->nombre="Dwayne";
    student11->apellido="Johnson";
    student11->nota=46;
    Student *student12 = (Student*)malloc(sizeof(Student));
    student12->nombre="Constantinos";
    student12->apellido="Daskalakis";
    student12->nota=100;
    Student *student13 = (Student*)malloc(sizeof(Student));
    student13->nombre="Zaha";
    student13->apellido="Hadid";
    student13->nota=71;
    Student *student14 = (Student*)malloc(sizeof(Student));
    student14->nombre="Claude";
    student14->apellido="Monet";
    student14->nota=61;
    Student *student15 = (Student*)malloc(sizeof(Student));
    student15->nombre="Pierre-August";
    student15->apellido="Renoir";
    student15->nota=53;
    Student *student16 = (Student*)malloc(sizeof(Student));
    student16->nombre="Auguste";
    student16->apellido="Rodin";
    student16->nota=77;
    Student *student17 = (Student*)malloc(sizeof(Student));
    student17->nombre="Nicanor";
    student17->apellido="Parra";
    student17->nota=88;
    Student *student18 = (Student*)malloc(sizeof(Student));
    student18->nombre="Charles";
    student18->apellido="Babbage";
    student18->nota=75;
    Student *student19 = (Student*)malloc(sizeof(Student));
    student19->nombre="Mario";
    student19->apellido="Benedetti";
    student19->nota=68;
    Student *student20 = (Student*)malloc(sizeof(Student));
    student20->nombre="Vicente";
    student20->apellido="Huidobro";
    student20->nota=90;
    Student *student21 = (Student*)malloc(sizeof(Student));
    student21->nombre="Marcel";
    student21->apellido="Proust";
    student21->nota=53;
    Student *student22 = (Student*)malloc(sizeof(Student));
    student22->nombre="Elon";
    student22->apellido="Musk";
    student22->nota=100;
    Student *student23 = (Student*)malloc(sizeof(Student));
    student23->nombre="Feodor";
    student23->apellido="Dostoyevski";
    student23->nota=49;
    Student *student24 = (Student*)malloc(sizeof(Student));
    student24->nombre="Georgios";
    student24->apellido="Papanikolaou";
    student24->nota=89;
    Student *student25 = (Student*)malloc(sizeof(Student));
    student25->nombre="Federico";
    student25->apellido="SantaMaria";
    student25->nota=55;
    push(&(stack2->top),student1);
    push(&(stack2->top),student2);
    push(&(stack2->top),student3);
    push(&(stack2->top),student4);
    push(&(stack2->top),student5);
    push(&(stack2->top),student6);
    push(&(stack2->top),student7);
    push(&(stack2->top),student8);
    push(&(stack2->top),student9);
    push(&(stack2->top),student10);
    push(&(stack2->top),student11);
    push(&(stack2->top),student12);
    push(&(stack2->top),student13);
    push(&(stack2->top),student14);
    push(&(stack2->top),student15);
    push(&(stack2->top),student16);
    push(&(stack2->top),student17);
    push(&(stack2->top),student18);
    push(&(stack2->top),student19);
    push(&(stack2->top),student20);
    push(&(stack2->top),student21);
    push(&(stack2->top),student22);
    push(&(stack2->top),student23);
    push(&(stack2->top),student24);
    push(&(stack2->top),student25);
}